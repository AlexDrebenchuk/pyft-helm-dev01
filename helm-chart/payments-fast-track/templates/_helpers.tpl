{{/*
Expand the name of the chart.
*/}}
{{- define "payments-fast-track.name" -}}
{{- default .Chart.Name .Values.paymentsFastTrack.nameOverride | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "payments-fast-track.fullname" -}}
{{- if .Values.paymentsFastTrack.fullnameOverride -}}
{{- .Values.paymentsFastTrack.fullnameOverride | trunc 63 | trimSuffix "-" -}}
{{- else -}}
{{- $name := default .Chart.Name .Values.paymentsFastTrack.nameOverride -}}
{{- if contains $name .Release.Name -}}
{{- .Release.Name | trunc 63 | trimSuffix "-" -}}
{{- else -}}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" -}}
{{- end -}}
{{- end -}}
{{- end -}}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "payments-fast-track.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Common labels
*/}}
{{- define "payments-fast-track.labels" -}}
helm.sh/chart: {{ include "payments-fast-track.chart" . }}
{{ include "payments-fast-track.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{/*
Selector labels
*/}}
{{- define "payments-fast-track.selectorLabels" -}}
app.kubernetes.io/name: {{ include "payments-fast-track.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}