## Payments fast track helm chart

For any changes required please contact francois.colombani@kyriba.com 

### Installation:

```console
$ helm install --namespace <desired_namespace> helm-kyriba-ci/payments-fast-track -f <path_to_values_override_file>.yaml
```
### Heath endpoint
```curl 'http://host:port/actuator/health' -i -X GET```

### Installation on kubernetes cluster manually:

1. Export credentials to artifactory or set it for your bash profile:
- `ARTIFACTORY_USER`
- `ARTIFACTORY_PASSWORD`

Note: Do not use Artifactory Oauth credentials

2. Install helmfile
- ```brew install helmfile```

3. Edit `helm-chart/payments-fast-track/helmfile/helmfile.yaml` file with next points:
- set createNamespace to false

4. Edit `helm-chart/payments-fast-track/templates/payments-fast-track.yaml` file with next points:
- delete block
```
{{- with .Values.imagePullSecrets }}
    imagePullSecrets:
        {{- toYaml . | nindent 8 }}
{{- end }}
```

5. Edit `helm-chart/payments-fast-track/values.yaml` file with next points:
- update image repository path by adding `snap`:
  (`artifactory.kod.kyriba.com/payments-fast-track-docker/snap/payments-fast-track`)
- update image tag. Tag can be found in CI logs on Container step

6. Goto `helm-chart/payments-fast-track/helmfile` directory

7. Start deploying process:
```
helmfile -e default deps
helmfile -f helmfile.yaml -n py-ft -e default template
helmfile -f helmfile.yaml -n py-ft -e default sync
```

8. Start testing process:
```
helmfile -f helmfile.yaml -n py-ft -e default test
```

9. Delete everything that was created
```
helmfile -f helmfile.yaml -n py-ft -e default destroy
```

### FAQ
- Payment Fast Track documentation is [here](https://kyridev.atlassian.net/wiki/spaces/PaymentFT/overview)
- More details about deployment [here](https://kyridev.atlassian.net/wiki/spaces/PaymentFT/pages/13042909570/Deployment+on+Kubernetes)
- More details about CI/CD process [here](https://kyridev.atlassian.net/wiki/spaces/DP/pages/12516131180/CI+CD+process)
- [Helmfile documentation](https://github.com/roboll/helmfile#configuration)