cd /D "%~dp0"

set NAME_SPACE=py-ft
set ENV_ID=%NAME_SPACE%
set CREATE_NAMESPACE=false

kubectl config use-context arn:aws:eks:us-east-1:667083570110:cluster/CI-DEV-01

if errorlevel 1 echo An error occurred & pause

call helmfile -e default deps

if errorlevel 1 echo An error occurred & pause

call helmfile -e default -f helmfile.yaml -n %NAME_SPACE% template

if errorlevel 1 echo An error occurred & pause

call helmfile -e default -f helmfile.yaml -n %NAME_SPACE% sync
cmd