#!/bin/bash

export NAME_SPACE=py-ft
export ENV_ID=$NAME_SPACE
export CREATE_NAMESPACE=false

kubectl config use-context arn:aws:eks:us-east-1:667083570110:cluster/CI-DEV-01;
if [[ $? -ne 0 ]]
then
  echo 'An error occurred'
  exit 1;
fi

helmfile -e default deps
if [[ $? -ne 0 ]]
then
  echo 'An error occurred'
  exit 1;
fi

helmfile -e default -f helmfile.yaml -n $NAME_SPACE template
if [[ $? -ne 0 ]]
then
  echo 'An error occurred'
  exit 1;
fi

helmfile -e default -f helmfile.yaml -n $NAME_SPACE sync
if [[ $? -ne 0 ]]
then
  echo 'An error occurred'
  exit 1;
fi